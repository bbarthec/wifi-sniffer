# Wifi Sniffer #

RaspberryPi based application which is sniffing local network using ARP packets for presence of selected devices with support of Ubidots IoT platform

### How do I get set up? ###

* Pull repo
* Install dependencies
```bash
sudo apt install python-setuptools
sudo easy_install pip
sudo pip install scapy
sudo pip install ubidots

```
* fill dictionary.csv with pairs `indicator,COLON_SEPARATED_MAC_ADDRESS
* pass your `ubidots key` to the program modifying the source code global variable